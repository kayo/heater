/* common config */
#define mcu_frequency 48
#define systick_config 1,100

/* pcd8544 config */
#define pcd8544_spi 1,16 /* device,divider */
#define pcd8544_dma 1,3 /* controller,channel */
#define pcd8544_dc F,0 /* port,pad */
#define pcd8544_res F,1 /* port,pad */
#define pcd8544_cs A,6 /* port,pad */
#define pcd8544_bl_pwm 14,1 /* timer,channel */
//#define pcd8544_bl A,8 /* port,pad */
#define bitmap_config 84,48,0,0 /* left,top,right,bottom */

/* encoder handler */
#define evt_enter 1
#define evt_setup 2
#define evt_incr 3
#define evt_decr 4
#define evt_tick 5

#define btn_config A,3,0,_(evt_enter,100,1000)_(evt_setup,2000,5000) /* port,pad,active level,_(event,interval mS)... */
#define enc_config (A,1,0),(A,2,0),evt_incr,evt_decr /* (port A,pad A,active level A),(port B,pad B,active level B),cw event,ccw event */

/* pcd8544 backlight pwm */
#define pwm1_config 14,0,1<<12,(pp,nc,nc,nc),_AF4 /* timer,prescaler,period,(channels configs),remap */

#define sensor_adc 1,12,1.2,3.3 /* adc device,adc bits,adc Vref,adc Vtop */
#define sensor_dma 1,1 /* device,channel */
#define therm_input B,1,9 /* port,pad,channel */

//#define filter_config 16,8 /* average samples,median samples */
#define filter_config 24,8 /* average samples,median samples */

//#define therm_config 4,(4.7e3,0),SH,(0.0013081992342927878,0.00005479611105636211,9.415113523239857e-7) /* fraction bits,resistors(r1,r2),model,params(a,b,c) */
#define therm_config 0,(4.7e3,0),BETA,(3450,100e3,C2K(24)) /* fraction bits,resistors(r1,r2),model,params(beta,r0,t0) */

//#define vref_config 10 /* fraction bits */
//#define tmcu_config 4,30,1.43,4.3 /* fraction bits,T0,Vsense(T0),Avg Slope */

/* zero-detector */
#define zero_det_input A,0 /* port,pad */

#define fan_ctl_output A,10,0,2 /* port,pad,active level,alternative function */
#define fan_ctl_timer 1,3 /* device,channel */

#define heat_ctl_output A,9,0,2 /* port,pad,active level,alternative function */
#define heat_ctl_timer 1,2 /* device,channel */

#define spi1_remap _AF0
#define tim14_remap _AF4

/**
 * Compatibility with STM32F0
 */

#define GPIO_BANK_SPI1_AF0_MOSI GPIOA
#define GPIO_SPI1_AF0_MOSI GPIO7

#define GPIO_BANK_SPI1_AF0_SCK GPIOA
#define GPIO_SPI1_AF0_SCK GPIO5

#define GPIO_BANK_TIM14_AF4_CH1 GPIOA
#define GPIO_TIM14_AF4_CH1 GPIO4

#define NVIC_DMA1_CHANNEL3_IRQ NVIC_DMA1_CHANNEL2_3_IRQ
#define dma1_channel3_isr dma1_channel2_3_isr

#ifdef NVIC_EXTI0_1_IRQ
#define NVIC_EXTI0_IRQ NVIC_EXTI0_1_IRQ
#define NVIC_EXTI1_IRQ NVIC_EXTI0_1_IRQ

void exti0_isr(void) __attribute__((weak));
void exti1_isr(void) __attribute__((weak));
#endif

#ifdef NVIC_EXTI2_3_IRQ
#define NVIC_EXTI2_IRQ NVIC_EXTI2_3_IRQ
#define NVIC_EXTI3_IRQ NVIC_EXTI2_3_IRQ

void exti2_isr(void) __attribute__((weak));
void exti3_isr(void) __attribute__((weak));
#endif

#define NVIC_TIM1_IRQ NVIC_TIM1_BRK_UP_TRG_COM_IRQ
#define tim1_isr tim1_brk_up_trg_com_isr
