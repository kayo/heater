#ifndef __PANEL_H__
#define __PANEL_H__ "panel.h"

void panel_init(void);
void panel_done(void);
void panel_step(void);

#endif /* __PANEL_H__ */
