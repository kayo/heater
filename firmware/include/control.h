#ifndef __CONTROL_H__
#define __CONTROL_H__

#include <stdint.h>

typedef enum {
  state_off,
  state_on,
  state_tune,
  state_blow,
} state_t;

typedef struct {
#if USE_SENSOR
  struct {
    int16_t temp;
  } sensor;
#endif
  struct {
    int16_t fan;
    int16_t heat;
  } driver;
  state_t state;
} control_t;

extern control_t control;

#endif /* __CONTROL_H__ */
