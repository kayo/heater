#ifndef __SENSOR_H__
#define __SENSOR_H__ "sensor.h"

/*
typedef struct {
  int16_t therm;
  int16_t t_mcu;
  int16_t v_ref;
} sensor_data_t;
*/

void sensor_init(void);
void sensor_done(void);

void sensor_resume(void);

/*void sensor_read(sensor_data_t *data);*/

int16_t sensor_therm(void);

#endif /* __SENSOR_H__ */
