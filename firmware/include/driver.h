#ifndef __DRIVER_H__
#define __DRIVER_H__

void driver_init(void);
void driver_done(void);

void driver_fan(int16_t val);
void driver_heat(int16_t val);

#endif /* __DRIVER_H__ */
