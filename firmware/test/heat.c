#include <stdint.h>
#include <stdio.h>

#define HEAT_BITS 6
#define HEAT_BASE ((1 << HEAT_BITS))

static struct {
  uint8_t count;
  uint8_t duty;
} heat = { 0, 0 };

#define HEAT_MIN 0
#define HEAT_MAX 40

static void driver_heat(int16_t val) {
  heat.duty = (val - HEAT_MIN) * HEAT_BASE / (HEAT_MAX - HEAT_MIN);
  heat.count = 0;
}

#define heat_on() printf("*")
#define heat_off() printf(" ")

static void heat_trigger(void) {
  if (heat.duty & (1 << 6)) {
  on:
    heat_on();
    goto end;
  }

  uint16_t i = 0;

  for (; i < HEAT_BITS; i++) {
    if ((heat.duty & (1 << (5 - i))) && (heat.count & ((1 << (i + 1)) - 1)) == (1 << i)) {
      goto on;
    }
  }

  heat_off();
  
 end:
  if (heat.count < HEAT_BASE) {
    heat.count ++;
  } else {
    heat.count = 0;
  }
}

int main(void) {
  int i;

#define test(x)                  \
  heat.count = 0;                \
  driver_heat(x);                \
  printf("duty: %2u mod: %2u [", \
         x, heat.duty);          \
  for (i = 0; i < HEAT_BASE;     \
       i++) {                    \
    heat_trigger();              \
  }                              \
  printf("]\n");

  /*
  test(0);
  test(40);
  test(20);
  test(10);
  test(30);
  test(1);
  test(39);
  test(2);
  test(38);
  */

  int n;

  for (n = 0; n <= 40; n ++) {
    test(n);
  }
  
  return 0;
}
