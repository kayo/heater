#include <libopencm3/cm3/nvic.h>
#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/exti.h>

#include "macro.h"
#include "systick.h"
#include "monitor.h"
#include "panel.h"
#include "config.h"
//#include "delay.h"
#include "pwm.h"
#include "ticks.h"
#include "evt.h"

#if USE_MEMCHECK
#include "memtool.h"
#endif

#if USE_PARAMS
#include "params.h"
#endif

#if USE_SENSOR
#include "sensor.h"
#endif

#include "driver.h"

#include "control.h"

monitor_def();

pwm_def(1);

control_t control = {
  {0},
  {0, 0},
  state_off
};

/**
 * Compatibility with STM32F0
 */
#ifdef NVIC_EXTI0_1_IRQ
void exti0_1_isr(void) {
  uint32_t status = exti_get_flag_status(EXTI0 | EXTI1);

  if (status & EXTI0) {
    exti0_isr();
  }
  
  if (status & EXTI1) {
    exti1_isr();
  }
}
#endif /* <NVIC_EXTI0_1_IRQ */

#ifdef NVIC_EXTI2_3_IRQ
void exti2_3_isr(void) {
  uint32_t status = exti_get_flag_status(EXTI2 | EXTI3);

  if (status & EXTI2) {
    exti2_isr();
  }
  
  if (status & EXTI3) {
    exti3_isr();
  }
}
#endif /* <NVIC_EXTI2_3_IRQ */

static inline void hardware_init(void) {
  /* Enable GPIO clock */
  rcc_periph_clock_enable(RCC_GPIOA);
  rcc_periph_clock_enable(RCC_GPIOB);
  rcc_periph_clock_enable(RCC_GPIOF);
  
  /* Enable DMA1 clock */
  rcc_periph_clock_enable(RCC_DMA);
}

static inline void hardware_done(void) {
  /* Disable DMA1 clock */
  rcc_periph_clock_disable(RCC_DMA);

  /* Disable GPIO clock */
  rcc_periph_clock_disable(RCC_GPIOA);
  rcc_periph_clock_disable(RCC_GPIOB);
  rcc_periph_clock_disable(RCC_GPIOF);
}

static inline void init(void) { 
  rcc_clock_setup_in_hsi_out_48mhz();
  
  //delay_init();
  hardware_init();
  pwm1_init();
  systick_init();
  panel_init();

#if USE_SENSOR
  sensor_init();
#endif
  
  driver_init();
}

#if 0
static inline void done(void) {
  driver_done();
  sensor_done();
  panel_done();
  
#if USE_SENSOR
  systick_done();
#endif
  
  pwm1_done();
  hardware_done();
}
#else
#define done()
#endif

static ticks_t ticks = 0;

ticks_t ticks_read(void) {
  return ticks;
}

#if USE_MEMCHECK
static mem_info_t mem_info;
#endif

int main(void) {
#if USE_MEMCHECK
  mem_prefill();
#endif
  
  init();

#if USE_PARAMS
  param_init(&params);
#endif
  
  monitor_init();
  
  for (;;) {
    monitor_wait();
    ticks += _NTH1(systick_config);

#if USE_SENSOR
    if (!(ticks % 1000)) {
      evt_push(evt_tick);
    }
#endif

    panel_step();

#if USE_MEMCHECK
    mem_measure(&mem_info);
#endif
  }
  
  done();
  
  return 0;
}
