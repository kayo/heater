#include "panel.h"

#include "control.h"
#include "persist.h"
#include "pcd8544.h"
#include "bitmap.h"

#include "driver.h"
#include "sensor.h"

#include "macro.h"
#include "config.h"

#include "btn.h"
#include "enc.h"

btn_def();
enc_def();

static bitmap_state_t canvas_state;

static const bitmap_canvas_t canvas = {
  pcd8544_frame_buffer,
  { {{ _NTH0(bitmap_config), _NTH1(bitmap_config) }},
    {{ _NTH2(bitmap_config), _NTH3(bitmap_config) }} },
  &canvas_state
};

#define progress_base 1024
#define from_range(x, a, b) (((x) - (a)) * progress_base / ((b) - (a)))

static void
draw_process(const bitmap_canvas_t *const c,
             bitmap_coord_t x,
             bitmap_coord_t y,
             bitmap_coord_t w,
             bitmap_coord_t h,
             uint16_t value) {
  bitmap_coord_t X = x + w;
  bitmap_coord_t Y = y + h;
  
  bitmap_rect(c, x, y, X, Y);
  bitmap_primitive(c, bitmap_fill);
  bitmap_rect(c, x + 1, y + 1, value * (X - 1) / progress_base, Y - 1);
  bitmap_primitive(c, bitmap_line);
}

typedef uint16_t uint_t;
typedef int16_t sint_t;

static int utos_dig(char **ptr, char *end, uint_t val, uint_t dig){
  char *p = *ptr, *q = p + dig;
  char t;
  
  do {
    if (p == end) {
      return 0; /* nothing */
    }
    *p++ = (val % 10) + '0';
    val /= 10;
  } while (val > 0);

  while (p < q) {
    if (p == end) {
      return 0; /* nothing */
    }
    *p++ = '0';
  }
  
  q = *ptr;
  *ptr = p--;
  
  while (q < p) { /* reverse digits */
    t = *q;
    *q++ = *p;
    *p-- = t;
  }
  
  return 1;
}

#define utos(ptr, end, val) utos_dig(ptr, end, val, 0)

static int itos_dig(char **ptr, char *end, sint_t val, uint_t dig){
  if (val < 0 && *ptr != end) {
    *(*ptr)++ = '-';
    val = -val;
  }
  
  return utos_dig(ptr, end, val, dig);
}

#define itos(ptr, end, val) itos_dig(ptr, end, val, 0)

static void bitmap_int16(const bitmap_canvas_t *bitmap,
                         bitmap_coord_t x,
                         bitmap_coord_t y,
                         int8_t digits,
                         int8_t point,
                         int16_t value) {
  char str[digits + 1];
  char *ptr = str;

  if (!point) {
    itos(&ptr, str + digits, value);
  } else {
    uint16_t fract = 10;
    uint8_t i = 0;
    for (; i < point - 1; i++) fract *= 10;
    
    itos(&ptr, ptr + digits - (point + 1), value / fract);
    *ptr++ = '.';
    
    itos_dig(&ptr, ptr + point, value % fract, point);
  }
  
  *ptr = '\0';
  
  bitmap_text(bitmap, x + (digits - (ptr - str)) * 8, y, str);
}

static void
display_draw(void) {
  for (; pcd8544_active(); );
  
  bitmap_operation(&canvas, bitmap_clear);
  bitmap_apply(&canvas);
  
  bitmap_operation(&canvas, bitmap_draw);

#if USE_SENSOR
  bitmap_text(&canvas, 0, 0, "Темп.   °C");
  bitmap_int16(&canvas, 8*5, 0, 3, 0, control.sensor.temp);
#endif
  
  bitmap_text(&canvas, 0, 10, "Вент.    %");
  bitmap_int16(&canvas, 8*5, 10, 4, 0, control.driver.fan);
  draw_process(&canvas, 0, 19, 82, 8, from_range(control.driver.fan, FAN_MIN, FAN_MAX));
  
  bitmap_text(&canvas, 0, 30, "Нагр.   kW");
  bitmap_int16(&canvas, 8*5, 30, 3, 1, control.driver.heat);
  draw_process(&canvas, 0, 39, 82, 8, from_range(control.driver.heat, HEAT_MIN, HEAT_MAX));
  
  pcd8544_update();
}

static void
display_on(void) {
  pcd8544_init();
  
  bitmap_init(&canvas);
  
  bitmap_primitive(&canvas, bitmap_line);
  bitmap_align(&canvas, bitmap_top | bitmap_left);
  bitmap_font(&canvas, font_8x8);
  
  display_draw();

  pcd8544_backlight(100);
  //pwm1_set1((1<<11)-1);
}

static void
display_off(void) {
  pcd8544_backlight(0);
  
  pcd8544_done();
}

void panel_init(void) {
  btn_init();
  enc_init();
}

void panel_done(void) {
  btn_done();
  enc_done();
}

void panel_step(void) {
  evt_t e;

  switch (control.state) {
  case state_off:
    evt_each(e) {
      switch (e) {
      case evt_enter:
      on:
        control.state = state_on;
        persist_load(&control.driver);
        driver_fan(control.driver.fan);
        driver_heat(control.driver.heat);
        display_on();
        break;
      }
    }
    break;
  case state_on:
    evt_each(e) {
      switch (e) {
      case evt_enter:
        control.state = state_blow;
        driver_heat(0);
        persist_save(&control.driver);
        display_off();
        break;
      case evt_setup:
        control.state = state_tune;
        break;
      case evt_incr:
        if (control.driver.heat < HEAT_CTL_MAX) {
          control.driver.heat += HEAT_CTL_STEP;
        }
        driver_heat(control.driver.heat);
        display_draw();
        break;
      case evt_decr:
        if (control.driver.heat > HEAT_CTL_MIN) {
          control.driver.heat -= HEAT_CTL_STEP;
        }
        driver_heat(control.driver.heat);
        display_draw();
        break;
      case evt_tick:
        control.sensor.temp = sensor_therm();
        display_draw();
        break;
      }
    }
    break;
  case state_tune:
    evt_each(e) {
      switch (e) {
      case evt_setup:
        control.state = state_on;
        display_draw();
        break;
      case evt_incr:
        if (control.driver.fan < FAN_CTL_MAX) {
          control.driver.fan += FAN_CTL_STEP;
        }
        driver_fan(control.driver.fan);
        display_draw();
        break;
      case evt_decr:
        if (control.driver.fan > FAN_CTL_MIN) {
          control.driver.fan -= FAN_CTL_STEP;
        }
        driver_fan(control.driver.fan);
        display_draw();
        break;
      case evt_tick:
        control.sensor.temp = sensor_therm();
        display_draw();
        break;
      }
    }
    break;
  case state_blow:
    evt_each(e) {
      switch (e) {
      case evt_enter:
        goto on;
        break;
      case evt_tick:
        control.sensor.temp = sensor_therm();
        if (control.sensor.temp < TEMP_BLOW) {
          driver_fan(0);
        }
        break;
      }
    }
  }
}
