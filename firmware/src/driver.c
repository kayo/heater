#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/exti.h>
#include <libopencm3/stm32/timer.h>
#include <libopencm3/cm3/nvic.h>

#include <math.h>

#include "sensor.h"
#include "driver.h"
#include "macro.h"
#include "config.h"
#include "lookup.h"

#define ZERO_DET_PORT _CAT2(GPIO, _NTH0(zero_det_input))
#define ZERO_DET_PAD _CAT2(GPIO, _NTH1(zero_det_input))
#define ZERO_DET_EXTI _CAT2(EXTI, _NTH1(zero_det_input))
#define ZERO_DET_IRQ _CAT3(NVIC_EXTI, _NTH1(zero_det_input), _IRQ)
#define zero_det_isr _CAT3(exti, _NTH1(zero_det_input), _isr)

#define FAN_CTL_PORT _CAT2(GPIO, _NTH0(fan_ctl_output))
#define FAN_CTL_PAD _CAT2(GPIO, _NTH1(fan_ctl_output))
#define FAN_CTL_ACT _NTH2(fan_ctl_output)

#ifndef STM32F1
#define FAN_CTL_AF _CAT2(GPIO_AF, _NTH3(fan_ctl_output))
#endif

#ifdef fan_ctl_timer
#define FAN_CTL_TIMER_RCC _CAT2(RCC_TIM, _NTH0(fan_ctl_timer))
#define FAN_CTL_TIMER _CAT2(TIM, _NTH0(fan_ctl_timer))
#define FAN_CTL_TIMER_OC _CAT2(TIM_OC, _NTH1(fan_ctl_timer))
#define FAN_CTL_TIMER_OIS _CAT2(TIM_CR2_OIS, _NTH1(fan_ctl_timer))
#define FAN_CTL_TIMER_IRQ _CAT3(NVIC_TIM, _NTH0(fan_ctl_timer), _IRQ)
#define fan_ctl_timer_isr _CAT3(tim, _NTH0(fan_ctl_timer), _isr)
#define FAN_CTL_BITS 12
#define FAN_CTL_WIDTH ((1<<(FAN_CTL_BITS))-1)
#define FAN_CTL_FREQ 100
#define FAN_CTL_DIV ((uint16_t)((1e6 * (mcu_frequency) / FAN_CTL_FREQ / (1<<(FAN_CTL_BITS)) * 2 + 1) / 2))
#define FAN_CTL_PERIOD ((uint16_t)(1e6 * (mcu_frequency) / FAN_CTL_FREQ / FAN_CTL_DIV))
#else/* !fan_ctl_timer */
#if FAN_CTL_ACT
#define fan_off gpio_clear
#define fan_on gpio_set
#else /* !FAN_CTL_ACT */
#define fan_off gpio_set
#define fan_on gpio_clear
#endif /* <FAN_CTL_ACT */
#endif /* <fan_ctl_timer */

#define HEAT_CTL_PORT _CAT2(GPIO, _NTH0(heat_ctl_output))
#define HEAT_CTL_PAD _CAT2(GPIO, _NTH1(heat_ctl_output))
#define HEAT_CTL_ACT _NTH2(heat_ctl_output)

#ifndef STM32F1
#define HEAT_CTL_AF _CAT2(GPIO_AF, _NTH3(heat_ctl_output))
#endif

#ifdef heat_ctl_timer
#define HEAT_CTL_TIMER_RCC _CAT2(RCC_TIM, _NTH0(heat_ctl_timer))
#define HEAT_CTL_TIMER _CAT2(TIM, _NTH0(heat_ctl_timer))
#define HEAT_CTL_TIMER_OC _CAT2(TIM_OC, _NTH1(heat_ctl_timer))
#define HEAT_CTL_TIMER_OIS _CAT2(TIM_CR2_OIS, _NTH1(heat_ctl_timer))
#define HEAT_CTL_TIMER_IRQ _CAT3(NVIC_TIM, _NTH0(heat_ctl_timer), _IRQ)
#define heat_ctl_timer_isr _CAT3(tim, _NTH0(heat_ctl_timer), _isr)
#define HEAT_CTL_BITS 12
#define HEAT_CTL_WIDTH ((1<<(HEAT_CTL_BITS))-1)
#define HEAT_CTL_FREQ 100
#define HEAT_CTL_DIV ((uint16_t)((1e6 * (mcu_frequency) / HEAT_CTL_FREQ / (1<<(HEAT_CTL_BITS)) * 2 + 1) / 2))
#define HEAT_CTL_PERIOD ((uint16_t)(1e6 * (mcu_frequency) / HEAT_CTL_FREQ / HEAT_CTL_DIV))
#else /* !heat_ctl_timer */
#if HEAT_CTL_ACT
#define heat_off gpio_clear
#define heat_on gpio_set
#else /* !HEAT_CTL_ACT */
#define heat_off gpio_set
#define heat_on gpio_clear
#endif /* <HEAT_CTL_ACT */
#endif /* <heat_ctl_timer */

void driver_init(void) {
  /* Enable EXTI0 interrupt */
  nvic_enable_irq(ZERO_DET_IRQ);

  /* Setup zero detector input */
#ifdef STM32F1
  gpio_set_mode(ZERO_DET_PORT,
                GPIO_MODE_INPUT,
                GPIO_CNF_INPUT_PULL_UPDOWN,
                ZERO_DET_PAD);
  /* pull-up */
  gpio_set(ZERO_DET_PORT, ZERO_DET_PAD);
#else /* !STM32F1 */
  gpio_mode_setup(ZERO_DET_PORT, GPIO_MODE_INPUT,
				  GPIO_PUPD_PULLUP, ZERO_DET_PAD);
#endif /* <STM32F1 */

  /* Configure the EXTI subsystem */
  exti_select_source(ZERO_DET_EXTI, ZERO_DET_PORT);
  exti_set_trigger(ZERO_DET_EXTI, EXTI_TRIGGER_FALLING);
  exti_enable_request(ZERO_DET_EXTI);

  /* Enable timer clock */
  rcc_periph_clock_enable(FAN_CTL_TIMER_RCC);

  /* Enable timer interrupt */
  nvic_enable_irq(FAN_CTL_TIMER_IRQ);

  /* Configure pump control timer */
  timer_reset(FAN_CTL_TIMER);

  /* Timer global mode */
  timer_set_mode(FAN_CTL_TIMER,
                 TIM_CR1_CKD_CK_INT, /* no divider */
                 TIM_CR1_CMS_EDGE, /* alignment edge */
#if FAN_CTL_ACT
                 TIM_CR1_DIR_UP
#else
				 TIM_CR1_DIR_DOWN
#endif
				 ); /* direction up */
  
  /* Configure fan ctl timer */
  timer_set_prescaler(FAN_CTL_TIMER, FAN_CTL_DIV - 1);
  timer_set_repetition_counter(FAN_CTL_TIMER, 0);
  timer_enable_preload(FAN_CTL_TIMER);
  timer_continuous_mode(FAN_CTL_TIMER);

#if HEAT_CTL_TIMER != FAN_CTL_TIMER
  timer_set_prescaler(HEAT_CTL_TIMER, HEAT_CTL_DIV - 1);
  timer_set_repetition_counter(HEAT_CTL_TIMER, 0);
  timer_enable_preload(HEAT_CTL_TIMER);
  timer_continuous_mode(HEAT_CTL_TIMER);
#endif

  /* Set period */
  timer_set_period(FAN_CTL_TIMER, FAN_CTL_PERIOD);
  
#if HEAT_CTL_TIMER != FAN_CTL_TIMER
  timer_set_period(HEAT_CTL_TIMER, HEAT_CTL_PERIOD);
#endif

  /* We need validate synchronization with HV wave */
  /* Enable update interrupt */
  timer_enable_irq(FAN_CTL_TIMER, TIM_DIER_UIE);

#if HEAT_CTL_TIMER != FAN_CTL_TIMER
  timer_enable_irq(HEAT_CTL_TIMER, TIM_DIER_UIE);
#endif

  /* Disable outputs */
#if 1
  timer_disable_oc_output(FAN_CTL_TIMER, TIM_OC1);
  timer_disable_oc_output(FAN_CTL_TIMER, TIM_OC2);
  timer_disable_oc_output(FAN_CTL_TIMER, TIM_OC3);
  timer_disable_oc_output(FAN_CTL_TIMER, TIM_OC4);
#endif

  /* Configure global mode of line */
  timer_disable_oc_clear(FAN_CTL_TIMER, FAN_CTL_TIMER_OC);
  timer_enable_oc_preload(FAN_CTL_TIMER, FAN_CTL_TIMER_OC);
  timer_set_oc_slow_mode(FAN_CTL_TIMER, FAN_CTL_TIMER_OC);
  timer_set_oc_mode(FAN_CTL_TIMER, FAN_CTL_TIMER_OC, TIM_OCM_PWM1);
  
#if FAN_CTL_ACT
  timer_reset_output_idle_state(FAN_CTL_TIMER, FAN_CTL_TIMER_OIS);
  timer_set_oc_polarity_high(FAN_CTL_TIMER, FAN_CTL_TIMER_OC);
  timer_set_oc_idle_state_unset(FAN_CTL_TIMER, FAN_CTL_TIMER_OC);
#else
  timer_set_output_idle_state(FAN_CTL_TIMER, FAN_CTL_TIMER_OIS);
  timer_set_oc_polarity_low(FAN_CTL_TIMER, FAN_CTL_TIMER_OC);
  timer_set_oc_idle_state_set(FAN_CTL_TIMER, FAN_CTL_TIMER_OC);
#endif

  timer_enable_oc_output(FAN_CTL_TIMER, FAN_CTL_TIMER_OC);

  /* Configure global mode of line */
  timer_disable_oc_clear(HEAT_CTL_TIMER, HEAT_CTL_TIMER_OC);
  timer_enable_oc_preload(HEAT_CTL_TIMER, HEAT_CTL_TIMER_OC);
  timer_set_oc_slow_mode(HEAT_CTL_TIMER, HEAT_CTL_TIMER_OC);
  timer_set_oc_mode(HEAT_CTL_TIMER, HEAT_CTL_TIMER_OC, TIM_OCM_PWM1);

#if HEAT_CTL_ACT
  timer_reset_output_idle_state(HEAT_CTL_TIMER, HEAT_CTL_TIMER_OIS);
  timer_set_oc_polarity_high(HEAT_CTL_TIMER, HEAT_CTL_TIMER_OC);
  timer_set_oc_idle_state_unset(HEAT_CTL_TIMER, HEAT_CTL_TIMER_OC);
#else
  timer_set_output_idle_state(HEAT_CTL_TIMER, HEAT_CTL_TIMER_OIS);
  timer_set_oc_polarity_low(HEAT_CTL_TIMER, HEAT_CTL_TIMER_OC);
  timer_set_oc_idle_state_set(HEAT_CTL_TIMER, HEAT_CTL_TIMER_OC);
#endif
  
  timer_enable_oc_output(HEAT_CTL_TIMER, HEAT_CTL_TIMER_OC);

  /* Turn off pump */
  driver_fan(0);
  driver_heat(0);

  timer_enable_break_main_output(FAN_CTL_TIMER);
#if HEAT_CTL_TIMER != FAN_CTL_TIMER
  timer_enable_break_main_output(HEAT_CTL_TIMER);
#endif

  /* To avoid low level pulse on pump control output we need:
	 1. Enable timer counter
	 2. Configure altfn output
	 3. Disable timer counter
  */
  timer_enable_counter(FAN_CTL_TIMER);
#if HEAT_CTL_TIMER != FAN_CTL_TIMER
  timer_enable_counter(HEAT_CTL_TIMER);
#endif
  
#ifdef STM32F1
  gpio_set_mode(FAN_CTL_PORT,
                GPIO_MODE_OUTPUT_50_MHZ,
#ifdef fan_ctl_timer
                GPIO_CNF_OUTPUT_ALTFN_OPENDRAIN,
#else
				GPIO_CNF_OUTPUT_OPENDRAIN,
#endif
                FAN_CTL_PAD);
#else /* !STM32F1 */
  gpio_mode_setup(FAN_CTL_PORT,
#ifdef fan_ctl_timer
				  GPIO_MODE_AF,
#else
				  GPIO_MODE_OUTPUT,
#endif
				  GPIO_PUPD_NONE, FAN_CTL_PAD);
  gpio_set_output_options(FAN_CTL_PORT, GPIO_OTYPE_OD,
						  GPIO_OSPEED_HIGH, FAN_CTL_PAD);
#ifdef fan_ctl_timer
  gpio_set_af(FAN_CTL_PORT, FAN_CTL_AF, FAN_CTL_PAD);
#endif
#endif /* <STM32F1 */
  
  timer_disable_counter(FAN_CTL_TIMER);

#ifdef STM32F1
  gpio_set_mode(HEAT_CTL_PORT,
                GPIO_MODE_OUTPUT_50_MHZ,
#ifdef heat_ctl_timer
				GPIO_CNF_OUTPUT_ALTFN_OPENDRAIN,
#else
                GPIO_CNF_OUTPUT_OPENDRAIN,
#endif
                HEAT_CTL_PAD);
#else /* !STM32F1 */
  gpio_mode_setup(HEAT_CTL_PORT,
#ifdef heat_ctl_timer
				  GPIO_MODE_AF,
#else
				  GPIO_MODE_OUTPUT,
#endif
				  GPIO_PUPD_NONE, HEAT_CTL_PAD);
  gpio_set_output_options(HEAT_CTL_PORT, GPIO_OTYPE_OD,
						  GPIO_OSPEED_HIGH, HEAT_CTL_PAD);
#ifdef heat_ctl_timer
  gpio_set_af(HEAT_CTL_PORT, HEAT_CTL_AF, HEAT_CTL_PAD);
#endif
#endif /* <STM32F1 */
}

void driver_done(void) {
  exti_disable_request(ZERO_DET_EXTI);

  /* Disable timer update interrupt */
  timer_disable_irq(FAN_CTL_TIMER, TIM_DIER_UIE);

  /* Disable timer interrupt */
  nvic_disable_irq(FAN_CTL_TIMER_IRQ);

#if HEAT_CTL_TIMER != FAN_CTL_TIMER
  /* Disable timer update interrupt */
  timer_disable_irq(HEAT_CTL_TIMER, TIM_DIER_UIE);

  /* Disable timer interrupt */
  nvic_disable_irq(HEAT_CTL_TIMER_IRQ);
#endif

  /* Disable EXTI0 interrupt */
  nvic_disable_irq(ZERO_DET_IRQ);
}

#ifndef heat_ctl_timer
#define HEAT_BASE (1 << HEAT_BITS)

static volatile struct {
  uint8_t count;
  uint8_t duty;
} heat = { 0, 0 };

void driver_heat(int16_t val) {
  heat.duty = (val - HEAT_MIN) * HEAT_BASE / (HEAT_MAX - HEAT_MIN);
  /*heat.count = 0;*/
}

static void heat_trigger(void) {
  if (heat.duty & (1 << 6)) {
  on:
    heat_on(HEAT_CTL_PORT, HEAT_CTL_PAD);
    goto end;
  }

  uint16_t i = 0;

  for (; i < HEAT_BITS; i++) {
    if ((heat.duty & (1 << (5 - i))) && (heat.count & ((1 << (i + 1)) - 1)) == (1 << i)) {
      goto on;
    }
  }

  heat_off(HEAT_CTL_PORT, HEAT_CTL_PAD);
  
 end:
  if (heat.count < HEAT_BASE) {
    heat.count ++;
  } else {
    heat.count = 0;
  }
}
#endif /* <heat_ctl_timer */

static volatile uint8_t hv_periods = 0;

void zero_det_isr(void) {
  exti_reset_request(ZERO_DET_EXTI);

#ifndef heat_ctl_timer
  heat_trigger();
#endif

  hv_periods = 0;
  timer_set_counter(FAN_CTL_TIMER, 0);
  timer_enable_counter(FAN_CTL_TIMER);

#if USE_SENSOR
  /* Trigger sensor */
  sensor_resume();
#endif
}

void fan_ctl_timer_isr(void) {
  timer_clear_flag(FAN_CTL_TIMER, TIM_SR_UIF);

#ifndef heat_ctl_timer
  heat_trigger();
#endif
  
  if (++hv_periods == 3) {
	timer_disable_counter(FAN_CTL_TIMER);
  }
}

#if HEAT_CTL_TIMER != FAN_CTL_TIMER
void heat_ctl_timer_isr(void) {
  timer_clear_flag(HEAT_CTL_TIMER, TIM_SR_UIF);
  
  if (++hv_periods == 3) {
	timer_disable_counter(HEAT_CTL_TIMER);
  }
}
#endif

/* linear power control */
/*
  Q[U]: sin(%pi * t);
  Q[P]: (1 - cos(%pi * t)) / 2;
  wxplot2d([Q[U], Q[P]], [t, 0, 1]);
  Q[C]: rhs(solve(x = Q[P], t)[1]);
  wxplot2d([Q[C]], [x, 0, 1]);
*/
/*
  EQ[p]: p=sin(%pi*t)^2;
  wxplot2d([rhs(EQ[p])], [t, 0, 1]);
  EQ[t]: solve(EQ[p], t)[2];
  wxplot2d([2*rhs(EQ[t])], [p, 0, 1]);
*/

lookup_table_defimpl(conv_dim, int16_t, uint16_t, uint8_t);

#define FAN_DIM_FROM 0
#define FAN_DIM_TO 100
#define FAN_DIM_STEP ((FAN_DIM_TO - FAN_DIM_FROM) / FAN_DIM_SIZE)

#define fan_dim(v) ((1.0 - acos(2 * (((double)(v) - FAN_DIM_FROM) / (FAN_DIM_TO - FAN_DIM_FROM)) - 1) / M_PI) * FAN_CTL_PERIOD)

#include lookup_table_file(fan_dim)
#define LOOKUP_FUNCTION(s) fan_dim(FAN_DIM_FROM + (s) * FAN_DIM_STEP)
lookup_table(conv_dim, fan_dim, FAN_DIM_FROM, FAN_DIM_STEP);
#undef LOOKUP_FUNCTION

void driver_fan(int16_t val) {
  uint16_t cmp = conv_dim_lookup(&fan_dim, val);
  
  if (cmp >= FAN_CTL_PERIOD) {
    cmp = FAN_CTL_PERIOD - 1;
  }
  
  /* Set the capture compare value for OC */
  timer_set_oc_value(FAN_CTL_TIMER, FAN_CTL_TIMER_OC, cmp);
}

#ifdef heat_ctl_timer
#define HEAT_DIM_FROM 0
#define HEAT_DIM_TO 40
#define HEAT_DIM_STEP ((HEAT_DIM_TO - HEAT_DIM_FROM) / HEAT_DIM_SIZE)

#define heat_dim(v) ((1.0 - acos(2 * (((double)(v) - HEAT_DIM_FROM) / (HEAT_DIM_TO - HEAT_DIM_FROM)) - 1) / M_PI) * HEAT_CTL_PERIOD)

#include lookup_table_file(heat_dim)
#define LOOKUP_FUNCTION(s) heat_dim(HEAT_DIM_FROM + (s) * HEAT_DIM_STEP)
lookup_table(conv_dim, heat_dim, HEAT_DIM_FROM, HEAT_DIM_STEP);
#undef LOOKUP_FUNCTION

void driver_heat(int16_t val) {
  uint16_t cmp = conv_dim_lookup(&heat_dim, val);
  
  if (cmp >= HEAT_CTL_PERIOD) {
    cmp = HEAT_CTL_PERIOD - 1;
  }
  
  /* Set the capture compare value for OC */
  timer_set_oc_value(HEAT_CTL_TIMER, HEAT_CTL_TIMER_OC, cmp);
}
#endif /* <heat_ctl_timer */
