#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/adc.h>
#include <libopencm3/stm32/dma.h>
#include <libopencm3/cm3/nvic.h>

#include <stdlib.h>
#include <math.h>

#include "sensor.h"
#include "macro.h"
#include "config.h"
#include "lookup.h"

#ifndef STM32F1
#define ADC_CHANNEL0 0
#define ADC_CHANNEL1 1
#define ADC_CHANNEL2 2
#define ADC_CHANNEL3 3
#define ADC_CHANNEL4 4
#define ADC_CHANNEL5 5
#define ADC_CHANNEL6 6
#define ADC_CHANNEL7 7
#define ADC_CHANNEL8 8
#define ADC_CHANNEL9 9
#endif

/* hardware defines */
#define THERM_PORT _CAT2(GPIO, _NTH0(therm_input))
#define THERM_PAD _CAT2(GPIO, _NTH1(therm_input))
#define THERM_ADC_CH _CAT2(ADC_CHANNEL, _NTH2(therm_input))

#define _ADC1 ADC1

#define ADC_RCC _CAT2(RCC_ADC, _NTH0(sensor_adc))
#define ADC_DEV _CAT2(_ADC, _NTH0(sensor_adc))
#define ADC_BITS _NTH1(sensor_adc)
#define ADC_VREF _NTH2(sensor_adc)
#define ADC_VTOP _NTH3(sensor_adc)

#define ADC_DMA _CAT2(DMA, _NTH0(sensor_dma))
#define ADC_DMA_CH _CAT2(DMA_CHANNEL, _NTH1(sensor_dma))
#define ADC_DMA_IRQ _CAT5(NVIC_DMA, _NTH0(sensor_dma), _CHANNEL, _NTH1(sensor_dma), _IRQ)
#define adc_dma_isr _CAT5(dma, _NTH0(sensor_dma), _channel, _NTH1(sensor_dma), _isr)

#ifdef tmcu_config
#define USE_TMCU 1
#define TMCU_FRAC_BITS _NTH0(tmcu_config)
#define TMCU_TEMPER_T0 _NTH1(tmcu_config)
#define TMCU_VSENSE_T0 _NTH2(tmcu_config)
#define TMCU_AVG_SLOPE _NTH3(tmcu_config)
#else
#define USE_TMCU 0
#endif

#ifdef vref_config
#define USE_VREF 1
#define VREF_FRAC_BITS _NTH0(vref_config)
#else
#define USE_VREF 0
#endif

#define THERM_FRAC_BITS _NTH0(therm_config)
#define THERM_RESISTORS _UNWR(_NTH1(therm_config))
#define THERM_MODEL _NTH2(therm_config)
#define THERM_PARAMS _UNWR(_NTH3(therm_config))

/* ADC filtering and conversion */
#ifdef filter_config
#define ADC_SAMPLES _NTH0(filter_config)
#define ADC_SELECT _NTH1(filter_config)

#define USE_AVERAGE_FILTER (ADC_SAMPLES > 0)
#define USE_MEDIAN_FILTER (ADC_SELECT > 0)
#else
#define USE_AVERAGE_FILTER 0
#define USE_MEDIAN_FILTER 0
#endif

#define ADC_OVER ((adc_value_t)(1 << ADC_BITS))
#define ADC_MAX ((adc_value_t)(ADC_OVER - 1))
#define ADC_REF ((adc_value_t)(ADC_VREF * ADC_MAX / 3.3))

typedef uint16_t adc_value_t;

typedef struct {
  adc_value_t therm;
#if USE_TMCU
  adc_value_t Tmcu;
#endif
#if USE_VREF
  adc_value_t Vref;
#endif
} adc_values_t;

static adc_values_t adc_buffer[ADC_SAMPLES];

#define ADC_CHANNELS (sizeof(adc_values_t)/sizeof(adc_value_t))

static adc_value_t adc_value_sample(const adc_value_t *field) {
#if USE_MEDIAN_FILTER
  adc_value_t values[ADC_SAMPLES], *value = values, *end = values + ADC_SAMPLES;
  
  for(; value < end; ){
    *value++ = *field;
    field += ADC_CHANNELS;
  }
  
  int adc_comp(const void *a, const void *b) {
    return *(const adc_value_t*)a < *(const adc_value_t*)b ? -1 : *(const adc_value_t*)a > *(const adc_value_t*)b ? 1 : 0;
  }
  
  qsort(values, ADC_SAMPLES, sizeof(adc_value_t), adc_comp);
  
#if USE_AVERAGE_FILTER
  uint32_t accum = 0;
  
  value = values + (ADC_SAMPLES-ADC_SELECT)/2;
  end = value + ADC_SELECT;
  
  for(; value < end; ){
    accum += *value++;
  }
  
  return accum / ADC_SELECT;
#else /* !USE_AVERAGE_FILTER */
  return values[ADC_SAMPLES / 2 + 1];
#endif /* <USE_AVERAGE_FILTER */

#else /* !USE_MEDIAN_FILTER */

#if USE_AVERAGE_FILTER
  uint64_t accum = 0;
  int i;
  
  for(i = 0; i < ADC_SAMPLES; i++){
    accum += *field;
    field += ADC_CHANNELS;
  }
  
  return accum / ADC_SAMPLES;
#else /* !USE_AVERAGE_FILTER */
  return *field;
#endif /* <USE_AVERAGE_FILTER */
  
#endif /* <USE_MEDIAN_FILTER */
}

static adc_values_t adc_values;

static void adc_convert(void) {
#if USE_TMCU
  adc_values.Tmcu = adc_value_sample(&adc_buffer->Tmcu);
#endif
#if USE_VREF
  adc_values.Vref = adc_value_sample(&adc_buffer->Vref);
#endif
  adc_values.therm = adc_value_sample(&adc_buffer->therm);
}

static adc_value_t adc_value(const adc_value_t *field) {
  adc_value_t value = *((const adc_value_t*)&adc_values + (field - (const adc_value_t*)&adc_buffer));
  return value;
}

#if USE_VREF
static inline int16_t sensor_Vref(void) {
  return ((uint32_t)adc_value(&adc_buffer->Vref) *
    (uint32_t)(ADC_VTOP) << VREF_FRAC_BITS) / ADC_OVER;
}
#endif

#if USE_TMCU
static inline int16_t sensor_Tmcu(void) {
  return ((int32_t)(TMCU_VSENSE_T0 * (1<<TMCU_FRAC_BITS) / TMCU_AVG_SLOPE) -
          (int32_t)adc_value(&adc_buffer->Tmcu) *
          (int32_t)(ADC_VTOP * (1<<TMCU_FRAC_BITS) / TMCU_AVG_SLOPE)) /
    ADC_OVER + (int16_t)(TMCU_TEMPER_T0 * (1<<TMCU_FRAC_BITS));
}
#endif

/**
 * @brief Convert ADC value to resistance.
 *
 *        Analog VCC
 *
 *            ^
 *            |
 *        .---o---.
 *        |       |
 *       .-.     .-.
 *       | |     | |
 *    R2 | |  Rt | |
 *       | |     | |
 *       '-'     '-'
 *        |       |
 *        '---o---o----> Analog IN
 *            |
 *           .-.
 *           | |
 *        R1 | |
 *           | |
 *           '-'
 *           _|_
 *
 *        Analog GND
 *
 * If r2 doesn't used, r2 must be set to 0
 */
#define A2R_HI_WITH_R2(A, R1, R2) (((double)(ADC_MAX) - (A)) * (R1) * (R2) / (((double)(A) - (ADC_MAX)) * (R1) + (R2)))
#define A2R_HI_WITHOUT_R2(A, R1) (((double)(ADC_MAX) - (A)) * (R1) / (double)(A))
#define A2R_HI_HELP(A, R1, R2) ((R2) == 0 ? A2R_HI_WITHOUT_R2(A, R1) : A2R_HI_WITH_R2(A, R1, R2))
#define A2R_HI(A, RS) A2R_HI_HELP(A, _NTH0(RS), _NTH1(RS))

#define R2A_HI_WITH_R2(R, R1, R2) ((((double)(ADC_MAX) * (R1) - (R)) * (R2) + (double)(ADC_MAX) * (R) * (R1)) / ((R1) * ((R2) + (R))))
#define R2A_HI_WITHOUT_R2(R, R1) ((double)(ADC_MAX) * (R1) / ((R1) + (R)))
#define R2A_HI_HELP(R, R1, R2) ((R2) == 0 ? R2A_HI_WITHOUT_R2(R, R1) : R2A_HI_WITH_R2(R, R1, R2))
#define R2A_HI(R, RS) R2A_HI_HELP(R, _NTH0(RS), _NTH1(RS))

/**
 * @brief Steinhart-Hart NTC thermistor model definition
 *
 * [e.illumium.org/thermistor](http://e.illumium.org/thermistor)
 */
#define SH_R2K_HELP(l, a, b, c) (1.0 / ((a) + (b) * (l) + (c) * pow((l), 3)))
#define SH_R2K(R, P...) SH_R2K_HELP(log(R), ##P)

/**
 * @brief Simplified beta NTC thermistor model definition
 *
 * [e.illumium.org/thermistor](http://e.illumium.org/thermistor)
 */
#define BETA_R2K_HELP(R, BETA, R0, T0) (1.0 / (1.0 / (T0) + 1.0 / (BETA) * log((R) / (R0))))
#define BETA_R2K(R, P) BETA_R2K_HELP(R, _NTH0(P), _NTH1(P), _NTH2(P))

#define BETA_K2R_HELP(K, BETA, R0, T0) (R0 * exp(BETA / K - BETA / T0))
#define BETA_K2R(K, P) BETA_K2R_HELP(R, _NTH0(P), _NTH1(P), _NTH2(P))

/**
 * @brief Kelvins to Celsius
 */
#define K2C(K) ((K)-273.15)

/**
 * @brief Celsius to Kelvins
 */
#define C2K(C) ((C)+273.15)

#define therm2adc(t) R2A_HI(_CAT2(THERM_MODEL, _K2R)(C2K((double)(t) / (1<<THERM_FRAC_BITS)), THERM_PARAMS), THERM_RESISTORS)
#define adc2therm(a) (K2C(_CAT2(THERM_MODEL, _R2K)(A2R_HI(a, THERM_RESISTORS), THERM_PARAMS)) * (1<<THERM_FRAC_BITS))

lookup_table_defimpl(conv_table, adc_value_t, int16_t, uint8_t);

#define THERM_RES_STEP ((THERM_RES_TO - THERM_RES_FROM) / THERM_RES_SIZE)

#include lookup_table_file(ntc100k)
//#define LOOKUP_FUNCTION(step) adc2therm(R2A_HI(THERM_RES_FROM, THERM_RESISTORS) + (step) * R2A_HI(((double)(THERM_RES_TO - THERM_RES_FROM) / THERM_RES_SIZE), THERM_RESISTORS))
#define LOOKUP_FUNCTION(s) adc2therm(THERM_RES_FROM + (s) * THERM_RES_STEP)
lookup_table(conv_table, ntc100k, THERM_RES_FROM, THERM_RES_STEP);
#undef LOOKUP_FUNCTION

int16_t sensor_therm(void) {
  return conv_table_lookup(&ntc100k, adc_value(&adc_buffer->therm));
}

/* (v / ADC_MAX - a / ADC_VTOP) / ((b - a) / ADC_VTOP) */
/* (v - a * ADC_MAX / ADC_VTOP) / ((b - a) * ADC_MAX / ADC_VTOP) */
#define adc_convert_range(v, a, b, l, h) ((((float)(v) - ((float)(a) * ADC_MAX / ADC_VTOP)) * (((float)(h) - (float)(l)) / (((float)(b) - (float)(a)) * ADC_MAX / ADC_VTOP))) + (float)(l))

void sensor_resume(void) {
  adc_start_conversion_regular(ADC_DEV);
}

static inline void sensor_suspend(void) {
  adc_power_off(ADC_DEV);
  adc_power_on(ADC_DEV);
}

/*
void sensor_read(sensor_data_t *data) {
  data->therm = sensor_therm();
  data->t_mcu = sensor_Tmcu();
  data->v_ref = sensor_Vref();
}
*/

void sensor_init(void) {
  /* Enable ADC_DEV clock */
  rcc_periph_clock_enable(ADC_RCC);
  
  /* Enable DMA channel interrupt */
  nvic_enable_irq(ADC_DMA_IRQ);

  /* Configure sensor GPIO */
	gpio_mode_setup(GPIOB, GPIO_MODE_ANALOG,
                  GPIO_PUPD_NONE,
                  THERM_PAD);

  /* Configure ADC subsystem */
  //adc_power_off(ADC_DEV);
  //for (; ADC_CR(ADC_DEV) & ADC_CR_ADDIS; );
  
  adc_calibrate_start(ADC_DEV);
  adc_calibrate_wait_finish(ADC_DEV);
  
  adc_set_clk_source(ADC_DEV, ADC_CLKSOURCE_ADC);
  
  /* ADC frequency (ADCCLK) is 24MHz/2 = 12MHz and period is 1/12 uS */
  
  /*adc_enable_external_trigger_regular(ADC_DEV, ADC_CR2_EXTSEL_SWSTART);*/
  adc_set_continuous_conversion_mode(ADC_DEV);
  adc_set_right_aligned(ADC_DEV);
  adc_set_resolution(ADC_DEV, ADC_RESOLUTION_12BIT);
  
  /* ADC conversion time for Tmcu (Tconv) is 239.5 + 12.5 = 252 cycles */
  /* 252 cycles / 12 MHz = 21 uS > 17.1 uS is ok */
  adc_set_sample_time_on_all_channels(ADC_DEV, ADC_SMPTIME_239DOT5);
  
  adc_enable_temperature_sensor();
  //adc_enable_scan_mode(ADC_DEV);
  {
    uint8_t channel[] = {
      THERM_ADC_CH,
#if USE_TMCU
      ADC_CHANNEL_TEMP,
#endif
#if USE_VREF
      ADC_CHANNEL_VREF
#endif
    };
    adc_set_regular_sequence(ADC_DEV, sizeof(channel)/sizeof(channel[0]), channel);
  }
  
  adc_power_on(ADC_DEV);

  { int i;
    for (i = 0; i < 800000; i++)    /* Wait a bit. */
      __asm__("nop");
  }
  
  adc_enable_dma(ADC_DEV);
  
  dma_channel_reset(ADC_DMA, ADC_DMA_CH);
  dma_set_priority(ADC_DMA, ADC_DMA_CH, DMA_CCR_PL_VERY_HIGH);
  dma_set_peripheral_size(ADC_DMA, ADC_DMA_CH, DMA_CCR_PSIZE_16BIT);
  dma_set_memory_size(ADC_DMA, ADC_DMA_CH, DMA_CCR_MSIZE_16BIT);
  dma_enable_memory_increment_mode(ADC_DMA, ADC_DMA_CH);
  dma_enable_circular_mode(ADC_DMA, ADC_DMA_CH);
  dma_set_read_from_peripheral(ADC_DMA, ADC_DMA_CH);
  dma_set_peripheral_address(ADC_DMA, ADC_DMA_CH, (uint32_t)&ADC_DR(ADC_DEV));
  dma_set_memory_address(ADC_DMA, ADC_DMA_CH, (uint32_t)&adc_buffer);
  dma_set_number_of_data(ADC_DMA, ADC_DMA_CH, sizeof(adc_buffer)/sizeof(adc_value_t));
  
  /* Enable transfer complete interrupt */
  dma_enable_transfer_complete_interrupt(ADC_DMA, ADC_DMA_CH);

  /* Start DMA transfer */
  dma_enable_channel(ADC_DMA, ADC_DMA_CH);
}

void sensor_done(void) {
  /* Disable the ADC_DEV */
  adc_power_off(ADC_DEV);

  /*
  gpio_set_mode(GPIOA, GPIO_MODE_INPUT,
                GPIO_CNF_INPUT_FLOAT,
                PHOTO_PAD
                | THERM_PAD
                | HUMID_PAD
                );
  */
  
  /* Disable the ADC_DMA channel1 */
  dma_disable_transfer_complete_interrupt(ADC_DMA, ADC_DMA_CH);
  dma_disable_channel(ADC_DMA, ADC_DMA_CH);

  /* Disable ADC_DMA channel1 interrupt */
  nvic_disable_irq(ADC_DMA_IRQ);
  
  /* Disable ADC_DEV clock */
  rcc_periph_clock_disable(ADC_RCC);
}

/* Initial adc conversion complete */
void adc_dma_isr(void) {
  dma_clear_interrupt_flags(ADC_DMA, ADC_DMA_CH, DMA_GIF | DMA_TCIF);

  sensor_suspend();
  adc_convert();
}
