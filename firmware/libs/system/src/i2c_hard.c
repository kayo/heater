#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/i2c.h>

#include "i2c.h"
#include "macro.h"
#include "config.h"
#include "delay.h"

#define I2C_NUM instance

i2c_def(I2C_NUM);

#define I2C_CONF _CAT3(i2c, I2C_NUM, _config)
#define I2C_DEV _CAT2(I2C, _NTH0(I2C_CONF))
#define I2C_RCC _CAT2(RCC_I2C, _NTH0(I2C_CONF))

#define I2C_MODE_std 0
#define I2C_MODE_fast 1

#define I2C_FAST_MODE _CAT2(I2C_MODE_, _NTH1(I2C_CONF))
#define I2C_FREQ_CONF _CAT3(I2C_CR2_FREQ_, _NTH2(I2C_CONF), MHZ)
#define I2C_DUTY_CONF _CAT2(I2C_CCR_DUTY_, _NTH3(I2C_CONF))

#if I2C_FAST_MODE
#define I2C_BASE_FREQ 400 /* kHz */
#define I2C_MAX_TRISE 300 /* nS */
#define I2C_MIN_TLOW 1300 /* nS */
#define I2C_MIN_THIGH 600 /* nS */
#else
#define I2C_BASE_FREQ 100 /* kHz */
#define I2C_MAX_TRISE 1000 /* nS */
#define I2C_MIN_TLOW 4700 /* nS */
#define I2C_MIN_THIGH 4000 /* nS */
#endif

/*
 * 36MHz / 400KHz = 90;
 * 36MHz / 100KHz = 360;
 */
#define I2C_CCR_CONF (_NTH2(I2C_CONF) * 1000 / I2C_BASE_FREQ)
/*
 * fclock for I2C is 36MHz ~> cycle time 28ns,
 * rise time for 400kHz => 300ns; 300ns/28ns ~ 11; Incremented by 1 -> 12.
 * rise time for 100kHz => 1000ns; 1000ns/28ns ~ 36.
 */
#define I2C_RISE_CONF (((I2C_MAX_TRISE * _NTH2(I2C_CONF) * 2 / 1000 + 1) / 2) + 1)

#define I2C_REMAP _NTH4(I2C_CONF)

#define I2C_SDA_PORT _CAT4(GPIO_BANK_I2C, _NTH0(I2C_CONF), I2C_REMAP, _SDA)
#define I2C_SDA_PAD _CAT4(GPIO_I2C, _NTH0(I2C_CONF), I2C_REMAP, _SDA)

#define I2C_SCL_PORT _CAT4(GPIO_BANK_I2C, _NTH0(I2C_CONF), I2C_REMAP, _SDA)
#define I2C_SCL_PAD _CAT4(GPIO_I2C, _NTH0(I2C_CONF), I2C_REMAP, _SDA)

static void i2c_flushout_(void);

static inline void
i2c_wait_start(uint32_t i2c) {
  for (; !(I2C_SR1(i2c) & I2C_SR1_SB); ) {
    if ((I2C_SR2(i2c) & (I2C_SR2_MSL | I2C_SR2_BUSY)) ==
        I2C_SR2_BUSY) {
      i2c_flushout_();
    }
  }
}

static inline void
i2c_wait_addr_tx(uint32_t i2c) {
  for (; !(I2C_SR1(i2c) & I2C_SR1_ADDR); (void)I2C_SR2(i2c));
}

static inline bool
i2c_master_mode_selected(uint32_t i2c) {
	uint32_t sr[2] = { I2C_SR1(i2c), I2C_SR2(i2c) };
  return (sr[0] & I2C_SR1_SB) && (sr[1] & (I2C_SR2_MSL | I2C_SR2_BUSY));
}

static inline bool
i2c_address_transmitted(uint32_t i2c) {
  uint32_t sr[2] = { I2C_SR1(i2c), I2C_SR2(i2c) };
  return sr[0] & I2C_SR1_ADDR;
}

static inline bool
i2c_master_transmitter_mode_selected(uint32_t i2c) {
  uint32_t sr[2] = { I2C_SR1(i2c), I2C_SR2(i2c) };
  return (sr[0] & (I2C_SR1_TxE | I2C_SR1_ADDR)) && (sr[1] & (I2C_SR2_MSL | I2C_SR2_BUSY | I2C_SR2_TRA));
}

static inline bool
i2c_master_receiver_mode_selected(uint32_t i2c) {
  uint32_t sr[2] = { I2C_SR1(i2c), I2C_SR2(i2c) };
  return (sr[0] & I2C_SR1_ADDR) && (sr[1] & (I2C_SR2_MSL | I2C_SR2_BUSY));
}

static inline bool
i2c_master_byte_transmitted(uint32_t i2c) {
  uint32_t sr[2] = { I2C_SR1(i2c), I2C_SR2(i2c) };
  return (sr[0] & (I2C_SR1_BTF | I2C_SR1_TxE)) && (sr[1] & (I2C_SR2_MSL | I2C_SR2_BUSY | I2C_SR2_TRA));
}

static inline bool
i2c_master_byte_received(uint32_t i2c) {
  uint32_t sr[2] = { I2C_SR1(i2c), I2C_SR2(i2c) };
  return (sr[0] & I2C_SR1_RxNE) && (sr[1] & (I2C_SR2_MSL | I2C_SR2_BUSY));
}

static inline void
i2c_transition_delay(void) {
	delay_us(
#if I2C_FAST_MODE
           2
#else
           5
#endif
           );
}

static void i2c_port_config(uint8_t cnf) {
#if I2C_SDA_PORT == I2C_SCL_PORT
  gpio_set_mode(I2C_SDA_PORT,
                GPIO_MODE_OUTPUT_50_MHZ,
                cnf,
                I2C_SDA_PAD | I2C_SCL_PAD);
#else
  gpio_set_mode(I2C_SDA_PORT,
                GPIO_MODE_OUTPUT_50_MHZ,
                cnf,
                I2C_SDA_PAD);
  gpio_set_mode(I2C_SCL_PORT,
                GPIO_MODE_OUTPUT_50_MHZ,
                cnf,
                I2C_SCL_PAD);
#endif
}

static bool i2c_flushout(void) {
	bool sda_stuck = false;
	int i;
	
	/* Reconfigure the GPIO pins as opendrain outputs (no alternate function) */
  i2c_port_config(GPIO_CNF_OUTPUT_OPENDRAIN);

	/* Release SDA and SCL pins (set it high) */
#if I2C_SDA_PORT == I2C_SCL_PORT
	gpio_set(I2C_SDA_PORT, I2C_SDA_PAD | I2C_SCL_PAD);
#else
  gpio_set(I2C_SDA_PORT, I2C_SDA_PAD);
  gpio_set(I2C_SCL_PORT, I2C_SCL_PAD);
#endif
  i2c_transition_delay();

	/* As per AN-686 Application Notes by Analog Devices, the maximum clock-through sequence
	 * length is 9 clocks (8 clocks for the data plus 1 for ACK/NOACK).
	 * One extra iteration is added to reuse the bus state checking code.
	 */
	for (i = 0; i < 10; i++) {
		/* Check SDA, it must be high as we set it above */
		sda_stuck = !gpio_get(I2C_SDA_PORT, I2C_SDA_PAD);
		
		if(!sda_stuck) break;
		
		/* SCL 1->0 transition */
		gpio_clear(I2C_SCL_PORT, I2C_SCL_PAD);
		i2c_transition_delay();
		
		/* SCL 0->1 transition */
		gpio_set(I2C_SCL_PORT, I2C_SCL_PAD);
		i2c_transition_delay();
	}

	/* Generate a STOP */

	/* Hold SDA (set it low) */
	gpio_clear(I2C_SDA_PORT, I2C_SDA_PAD);
	i2c_transition_delay();
	
	/* Release SDA (set it high)
	 * This creates a STOP condition (a low-to-high SDA transition while SCL is high)
	 */
	gpio_set(I2C_SDA_PORT, I2C_SDA_PAD);
	i2c_transition_delay();

  i2c_port_config(GPIO_CNF_OUTPUT_ALTFN_OPENDRAIN);
	
	return !sda_stuck;
}

static void i2c_flushout_(void) {
  /* Disable the I2C */
  i2c_peripheral_disable(I2C_DEV);
  
  /* Reconfigure the GPIO pins as opendrain outputs (no alternate function) */
  i2c_port_config(GPIO_CNF_OUTPUT_OPENDRAIN);

#if I2C_SDA_PORT == I2C_SCL_PORT
	gpio_set(I2C_SDA_PORT, I2C_SDA_PAD | I2C_SCL_PAD);
#else
  gpio_set(I2C_SDA_PORT, I2C_SDA_PAD);
  gpio_set(I2C_SCL_PORT, I2C_SCL_PAD);
#endif
  i2c_transition_delay();

#if I2C_SDA_PORT == I2C_SCL_PORT
	for (; gpio_get(I2C_SDA_PORT, I2C_SDA_PAD | I2C_SCL_PAD) != (I2C_SDA_PAD | I2C_SCL_PAD); );
#else
  for (; gpio_get(I2C_SDA_PORT, I2C_SDA_PAD) | gpio_get(I2C_SCL_PORT, I2C_SCL_PAD) != (I2C_SDA_PAD | I2C_SCL_PAD));
#endif

  gpio_clear(I2C_SDA_PORT, I2C_SDA_PAD);
  i2c_transition_delay();
  for (; gpio_get(I2C_SDA_PORT, I2C_SDA_PAD) == I2C_SDA_PAD; );

  gpio_clear(I2C_SCL_PORT, I2C_SCL_PAD);
  i2c_transition_delay();
  for (; gpio_get(I2C_SCL_PORT, I2C_SCL_PAD) == I2C_SCL_PAD; );
  
  gpio_set(I2C_SCL_PORT, I2C_SCL_PAD);
  i2c_transition_delay();
  for (; gpio_get(I2C_SCL_PORT, I2C_SCL_PAD) != I2C_SCL_PAD; );

  gpio_set(I2C_SDA_PORT, I2C_SDA_PAD);
  i2c_transition_delay();
  for (; gpio_get(I2C_SDA_PORT, I2C_SDA_PAD) != I2C_SDA_PAD; );

  /* Reconfigure the GPIO pins as opendrain outputs (no alternate function) */
  i2c_port_config(GPIO_CNF_OUTPUT_ALTFN_OPENDRAIN);

  //i2c_reset(I2C_DEV);
  I2C_CR1(I2C_DEV) |= I2C_CR1_SWRST;
  I2C_CR1(I2C_DEV) &= ~I2C_CR1_SWRST;

  /* Enable the I2C */
  i2c_peripheral_enable(I2C_DEV);
}

static void i2c_start_voodoo(uint32_t i2c, uint8_t addr) {
 start:
  /* Enable I2C acknowledge */
  i2c_enable_ack(i2c);
  
  /* Send START condition */
  i2c_send_start(i2c);
	
  /* Waiting for START is send and therefore switched to master mode */
  i2c_wait_start(i2c);
  /*
	uint32_t i;
	for (i = 0; !i2c_master_mode_selected(i2c); i++) {
		if (i > 0xffff) {
			i2c_flushout();
			goto start;
		}
	}
  */
	
	/* Send DS3231 slave address */
  i2c_send_7bit_address(i2c, addr, I2C_WRITE);
	
  /* Waiting for address is transferred */
	//while (!i2c_address_transmitted(i2c));
  i2c_wait_addr_tx(i2c);
}

static void i2c_stop_voodoo(uint32_t i2c) {
	/* Disable I2C acknowledgement */
  i2c_disable_ack(i2c);
	
	/* Send STOP condition */
  i2c_send_stop(i2c);
}

static void i2c_bus_reset(void) {
  #if I2C_SCL_PORT == I2C_SDA_PORT
  gpio_set(I2C_SDA_PORT, I2C_SCL_PAD | I2C_SDA_PAD);
#else
  gpio_set(I2C_SCL_PORT, I2C_SCL_PAD);
  gpio_set(I2C_SDA_PORT, I2C_SDA_PAD);
#endif
  
  /* Set opendrain for the SCL and SDA pins of I2C_DEV */
  i2c_port_config(GPIO_CNF_OUTPUT_OPENDRAIN);

  if (
#if I2C_SCL_PORT == I2C_SDA_PORT
      !gpio_get(I2C_SDA_PORT, I2C_SCL_PAD | I2C_SDA_PAD)
#else
      !gpio_get(I2C_SCL_PORT, I2C_SCL_PAD) ||
      !gpio_get(I2C_SDA_PORT, I2C_SDA_PAD)
#endif
      ) {
    i2c_flushout();
  }
  
  /* Set alternate function for the SCL and SDA pins of I2C_DEV */
  i2c_port_config(GPIO_CNF_OUTPUT_ALTFN_OPENDRAIN);
}

void i2c_fn(I2C_NUM, init)(void) {
    /* Enable clocks for I2C_DEV */
  rcc_periph_clock_enable(I2C_RCC);

  //i2c_reset(I2C_DEV);

  /* Set alternate functions for the SCL and SDA pins of I2C_DEV */
  i2c_port_config(GPIO_CNF_OUTPUT_ALTFN_OPENDRAIN);

  /* Disable the I2C before changing any configuration */
  i2c_peripheral_disable(I2C_DEV);

	/* Setup I2C clock */
  i2c_set_clock_frequency(I2C_DEV, I2C_FREQ_CONF);

#if I2C_FAST_MODE
  /* 400KHz - I2C Fast Mode */
  i2c_set_fast_mode(I2C_DEV);
#else
  /* 100KHz - I2C Fast Mode */
  i2c_set_standard_mode(I2C_DEV);
#endif
	
  i2c_set_dutycycle(I2C_DEV, I2C_DUTY_CONF);
  i2c_set_ccr(I2C_DEV, I2C_CCR_CONF/*90*/);
  i2c_set_trise(I2C_DEV, I2C_RISE_CONF/*11*/);

  i2c_bus_reset();
  
  /* Finally enable the peripheral */
  i2c_peripheral_enable(I2C_DEV);
}

void i2c_fn(I2C_NUM, done)(void) {
  /* Disable clocks for I2C_DEV */
  rcc_periph_clock_disable(I2C_RCC);
}

static inline void
i2c_write(uint32_t i2c,
          const uint8_t *ptr,
          uint8_t len) {
  const uint8_t *end = ptr + len - 1;

 write_byte:
	/* Send data byte */
	i2c_send_data(i2c, *ptr++);
  
  /* Waiting for data byte is transferred */
  while (!i2c_master_byte_transmitted(i2c));
  
  if (ptr <= end) {
    goto write_byte;
  }
}

static inline void
i2c_read(uint32_t i2c,
         uint8_t *ptr,
         uint8_t len) {
  uint8_t *end = ptr + len - 1;
  
 read_byte:
  /* Wait for byte received from slave */
	while (!i2c_master_byte_received(i2c));
  
  /* Receive the data byte */
	*ptr++ = i2c_get_data(i2c);
  
  if (ptr == end) {
    i2c_stop_voodoo(i2c);
  }
  
  if (ptr <= end) {
    goto read_byte;
  }
}

void i2c_fn(I2C_NUM, io)(uint8_t addr,
                         const uint8_t *wptr, uint8_t wlen,
                         uint8_t *rptr, uint8_t rlen) {
  if (wptr && wlen) {
    i2c_start_voodoo(I2C_DEV, addr);

    /* send data to slave */
    i2c_write(I2C_DEV, wptr, wlen);
  }

  if (rptr && rlen) {
    /* Send repeated START condition (aka Re-START) */
    i2c_send_start(I2C_DEV);
    
    /* Waiting for START is send and therefore switched to master mode */
    while (!i2c_master_mode_selected(I2C_DEV));
    
    /* Send DS3231 slave address for read from */
    i2c_send_7bit_address(I2C_DEV, addr, I2C_READ);
    
    /* Waiting for START is send and therefore switched to master mode */
    while (!i2c_master_receiver_mode_selected(I2C_DEV));
    
    i2c_read(I2C_DEV, rptr, rlen);
  } else {
    i2c_stop_voodoo(I2C_DEV);
  }
}

#if 0
static void i2c_read_buffer(uint32_t i2c, uint8_t addr, uint8_t reg, uint8_t *ptr, uint8_t len) {
  uint8_t *end = ptr + len - 1;
  
  i2c_start_voodoo(i2c, addr);
  
  /* Send DS3231 temperature MSB register address */
	i2c_send_data(i2c, reg);
	
  /* Wait for byte transmitted condition */
	while (!i2c_master_byte_transmitted(i2c));
  
  /* Send repeated START condition (aka Re-START) */
	i2c_send_start(i2c);

  /* Waiting for START is send and therefore switched to master mode */
	while (!i2c_master_mode_selected(i2c));

  /* Send DS3231 slave address for read from */
	i2c_send_7bit_address(i2c, addr, I2C_READ);
  
  /* Waiting for START is send and therefore switched to master mode */
	while (!i2c_master_receiver_mode_selected(i2c));

 read_byte:
  /* Wait for byte received from slave */
	while (!i2c_master_byte_received(i2c));
  
  /* Receive the data byte */
	*ptr++ = i2c_get_data(i2c);
  
  if (ptr == end) {
    i2c_stop_voodoo(i2c);
  }
  
  if (ptr <= end) {
    goto read_byte;
  }
}

static void i2c_write_buffer(uint32_t i2c, uint8_t addr, uint8_t reg, const uint8_t *ptr, uint8_t len) {
  const uint8_t *end = ptr + len - 1;
  
	i2c_start_voodoo(i2c, addr);
	
  /* Send DS3231 temperature MSB register address */
	i2c_send_data(i2c, reg);
  
  /* Wait for byte transmitted condition */
	while (!i2c_master_byte_transmitted(i2c));

 write_byte:
	/* Send data byte */
	i2c_send_data(i2c, *ptr++);
  
  /* Waiting for data byte is transferred */
  while (!i2c_master_byte_transmitted(i2c));
  
  if (ptr <= end) {
    goto write_byte;
  }
  
	i2c_stop_voodoo(i2c);
}
#endif
