#include <libopencm3/stm32/gpio.h>

#include "i2c.h"
#include "macro.h"
#include "config.h"
#include "delay.h"

#define I2C_NUM instance

i2c_def(I2C_NUM);

#define I2C_CONF _CAT3(i2c, I2C_NUM, _config)

#define I2C_MODE_std 0
#define I2C_MODE_fast 1

#define I2C_FAST_MODE _CAT2(I2C_MODE_, _NTH0(I2C_CONF))

#define I2C_SDA_PORT _CAT2(GPIO, _NTH1(I2C_CONF))
#define I2C_SDA_PAD _CAT2(GPIO, _NTH2(I2C_CONF))

#define I2C_SCL_PORT _CAT2(GPIO, _NTH3(I2C_CONF))
#define I2C_SCL_PAD _CAT2(GPIO, _NTH4(I2C_CONF))

static void
i2c_port_config(uint8_t dir,
                uint8_t cfg) {
#if I2C_SDA_PORT == I2C_SCL_PORT
  gpio_set_mode(I2C_SDA_PORT,
                dir,
                cfg,
                I2C_SDA_PAD | I2C_SCL_PAD);
#else
  gpio_set_mode(I2C_SDA_PORT,
                dir,
                cfg,
                I2C_SDA_PAD);
  gpio_set_mode(I2C_SCL_PORT,
                dir,
                cfg,
                I2C_SCL_PAD);
#endif
}

static inline void
i2c_pads_lo(void) {
  #if I2C_SDA_PORT == I2C_SCL_PORT
  gpio_clear(I2C_SDA_PORT,
             I2C_SDA_PAD | I2C_SCL_PAD);
#else
  gpio_clear(I2C_SDA_PORT,
             I2C_SDA_PAD);
  gpio_clear(I2C_SCL_PORT,
             I2C_SCL_PAD);
#endif
}

static inline void
i2c_pads_hi(void) {
  #if I2C_SDA_PORT == I2C_SCL_PORT
  gpio_set(I2C_SDA_PORT,
           I2C_SDA_PAD | I2C_SCL_PAD);
#else
  gpio_set(I2C_SDA_PORT,
           I2C_SDA_PAD);
  gpio_set(I2C_SCL_PORT,
           I2C_SCL_PAD);
#endif
}

#define i2c_sda_lo() gpio_clear(I2C_SDA_PORT, I2C_SDA_PAD)
#define i2c_sda_hi() gpio_set(I2C_SDA_PORT, I2C_SDA_PAD)
#define i2c_scl_lo() gpio_clear(I2C_SCL_PORT, I2C_SCL_PAD)
#define i2c_scl_hi() gpio_set(I2C_SCL_PORT, I2C_SCL_PAD)

void i2c_fn(I2C_NUM, init)(void) {
  i2c_pads_hi();
  
  i2c_port_config(GPIO_MODE_OUTPUT_50_MHZ,
                  GPIO_CNF_OUTPUT_OPENDRAIN);
}

void i2c_fn(I2C_NUM, done)(void) {
  i2c_port_config(GPIO_MODE_INPUT,
                  GPIO_CNF_INPUT_FLOAT);
}

static inline void
i2c_delay(void) {
	delay_us(
#if I2C_FAST_MODE
           2
#else
           5
#endif
           );
}

#define i2c_sda() gpio_get(I2C_SDA_PORT, I2C_SDA_PAD)

static void
i2c_start(void) {
  i2c_sda_hi();
  i2c_delay();
  i2c_scl_hi();
  i2c_delay();

  for (; !i2c_sda(); ) {
    i2c_scl_lo();
    i2c_delay();
    i2c_scl_hi();
    i2c_delay();
  }

  i2c_sda_lo();
  i2c_delay();
  i2c_scl_lo();
  i2c_delay();
}

static void
i2c_stop(void) {
  i2c_sda_lo();
  i2c_delay();
  i2c_scl_hi();
  i2c_delay();
  i2c_sda_hi();
  i2c_delay();
}

static uint8_t
i2c_write_byte(uint8_t data) {
  uint8_t i;
  uint8_t ack;

  for (i = 0; i < 8; i++) {
    if (data & 0x80) {
      i2c_sda_hi();
    } else {
      i2c_sda_lo();
    }
    i2c_delay();
    i2c_scl_hi();
    i2c_delay();
    i2c_scl_lo();
    //i2c_delay();
    data <<= 1;
  }

  i2c_sda_hi();
  i2c_delay();
  i2c_scl_hi();
  i2c_delay();

  ack = !i2c_sda();
  //i2c_delay();

  i2c_pads_lo();
  
  return ack;
}

static uint8_t
i2c_read_byte(uint8_t ack) {
  uint8_t i;
  uint8_t data = 0;

  i2c_sda_hi();

  for (i = 0; i < 8; i++) {
    i2c_delay();
    i2c_scl_hi();
    i2c_delay();

    data <<= 1;

    if (i2c_sda()) {
      data |= 1;
    }

    i2c_scl_lo();
  }

  if (ack) {
    i2c_sda_lo();
  }

  i2c_delay();
  i2c_scl_hi();
  i2c_delay();
  i2c_scl_lo();
  i2c_sda_hi();

  return data;
}

void i2c_fn(I2C_NUM, io)(uint8_t addr,
                         const uint8_t *wptr,
                         uint8_t wlen,
                         uint8_t *rptr,
                         uint8_t rlen) {
  addr <<= 1;
  
  if ((wptr && wlen) ||
      !(rptr && rlen)) {
    const uint8_t *wend = wptr + wlen;
    
    i2c_start();
    i2c_write_byte(addr);
    
    for (; wptr < wend; ) {
      i2c_write_byte(*wptr++);
    }
  }

  if (rptr && rlen) {
    uint8_t *rend = rptr + rlen - 1;
    
    i2c_start();
    i2c_write_byte(addr | 1);

    for (; rptr <= rend; rptr++) {
      *rptr = i2c_read_byte(rptr < rend);
    }
  }

  i2c_stop();
}
