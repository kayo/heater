#ifndef __CRC_H__
#define __CRC_H__ "crc.h"

void crc32_init(void);
void crc32_done(void);
void crc32_reset(void);
uint32_t crc32_update(const uint8_t *ptr, uint16_t len);

#endif /* __CRC_H__ */
