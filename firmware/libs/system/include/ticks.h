#ifndef __TICKS_H__
#define __TICKS_H__

#include <stdint.h>

typedef uint32_t ticks_t;

ticks_t ticks_read(void);
ticks_t ticks_diff(ticks_t from, ticks_t to);

#endif /* __TICKS_H__ */
