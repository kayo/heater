#include <libopencm3/cm3/nvic.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/exti.h>

#include "macro.h"
#include "config.h"

#include "btn.h"
#include "evt.h"
#include "ticks.h"

#ifdef instance
#define BTN_NUM instance
#else
#define BTN_NUM
#endif

btn_def(BTN_NUM);

#define BTN_CFG _CAT3(btn, BTN_NUM, _config)

#define BTN_PORT _CAT2(GPIO, _NTH0(BTN_CFG))
#define BTN_PAD _CAT2(GPIO, _NTH1(BTN_CFG))
#define BTN_EXTI _CAT2(EXTI, _NTH1(BTN_CFG))

#define BTN_ACT _NTH2(BTN_CFG) /* active state */
#define BTN_EVT _NTH3(BTN_CFG) /* events */

#define BTN_IRQ _CAT3(NVIC_EXTI, _NTH1(BTN_CFG), _IRQ)
#define btn_isr _CAT3(exti, _NTH1(BTN_CFG), _isr)

static bool pushed = false;
static ticks_t push_time = 0;

static void
btn_get(void) {
  pushed = !gpio_get(BTN_PORT, BTN_PAD) == !BTN_ACT;
  if (pushed) {
    push_time = ticks_read();
  }
}

void btn_fn(BTN_NUM, init)(void) {
  nvic_enable_irq(BTN_IRQ);
  
#ifdef STM32F1
  gpio_set_mode(BTN_PORT,
                GPIO_MODE_INPUT,
                GPIO_CNF_INPUT_PULL_UPDOWN,
                BTN_PAD);
  /* pull-up */
#if BTN_ACT
  gpio_clear(BTN_PORT, BTN_PAD);
#else
  gpio_set(BTN_PORT, BTN_PAD);
#endif
#else
  gpio_mode_setup(BTN_PORT, GPIO_MODE_INPUT,
                  BTN_ACT ? GPIO_PUPD_PULLDOWN :
                  GPIO_PUPD_PULLUP, BTN_PAD);
#endif
  
  exti_select_source(BTN_EXTI, BTN_PORT);
  exti_set_trigger(BTN_EXTI, EXTI_TRIGGER_BOTH);

  btn_get();
  
  exti_enable_request(BTN_EXTI);
}

void btn_fn(BTN_NUM, done)(void) {
  exti_disable_request(BTN_EXTI);

#ifdef STM32F1
  gpio_set_mode(BTN_PORT,
                GPIO_MODE_INPUT,
                GPIO_CNF_INPUT_FLOAT,
                BTN_PAD);
#else
  gpio_mode_setup(BTN_PORT, GPIO_MODE_INPUT,
                  GPIO_PUPD_NONE, BTN_PAD);
#endif

  nvic_disable_irq(BTN_IRQ);
}

void btn_isr(void) {
  exti_reset_request(BTN_EXTI);
  
  if (!gpio_get(BTN_PORT, BTN_PAD) == !BTN_ACT) {
    if (!pushed) {
      pushed = true;
      push_time = ticks_read();
    }
  } else {
    if (pushed) {
      pushed = false;
      ticks_t pull_time = ticks_read();
      ticks_t diff_time = ticks_diff(push_time, pull_time);
      
#define _(evt, from, to) if (diff_time >= (from) && diff_time < (to)) { evt_push(evt); } else
      BTN_EVT
#undef _
        {}
    }
  }
}
