#ifndef __EVT_H__
#define __EVT_H__ "evt.h"

#include <stdint.h>

enum { evt_none = 0 };
typedef uint8_t evt_t;

evt_t evt_pull(void);
void evt_push(evt_t evt);

#define evt_each(e) for (; evt_none != (e = evt_pull()); )

#endif /* __EVT_H__ */
